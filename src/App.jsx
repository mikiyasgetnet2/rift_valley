import React from 'react'
import DrawerAppBar from './Components/Nav';
import Home from './Components/Home';
import About from './Components/About';
import MissionAndVision from './Components/Misson';
import Footer from './Components/Fotter';
import Contact from './Components/Contact';
import Blog from './Components/Blog';
const App = () => {
  return (
    <div>
      <DrawerAppBar/>
      <Home/>
      <About/>
      <MissionAndVision/>
      <Blog/>
      <Contact/>
      <Footer/>
    </div>
  )
}

export default App