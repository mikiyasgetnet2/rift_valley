import React from 'react';
import { Card, CardContent, Typography, Button, Divider } from '@mui/material';

const Blog = () => {
  // Sample blog data
  const blogs = [
    { id: 1, author: 'John Doe', message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' },
    { id: 2, author: 'Jane Smith', message: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.' },
    { id: 3, author: 'Alice Johnson', message: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.' }
  ];

  // Random image URL
  const randomImageURL = 'https://source.unsplash.com/random/50x50';

  // Render blog cards
  const renderBlogs = () => {
    return blogs.map((blog, index) => (
     index == 2?
     <div style={{ display: 'flex', justifyContent: 'center' }}>
     <Card key={blog.id} sx={{ backgroundColor: '#f0f0f0', opacity: 0.3, marginBottom:'40px',width: '900px' }}>
     <CardContent style={{ display: 'flex', alignItems: 'center' }}>
       <img src={randomImageURL} alt="Author" style={{ width: '100px', height: '100px', marginRight: '10px', borderRadius: '50%' }} />
       <div>
         <Typography variant="h6" gutterBottom>
           {blog.author}
         </Typography>
         <Typography variant="body1" sx={{ color: '#333', marginBottom: '12px' }}>
           {blog.message}
         </Typography>
       </div>
     </CardContent>
   </Card>
   </div>: <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Card key={blog.id} sx={{ backgroundColor: '#f0f0f0', marginBottom: '16px', width: '900px' }}>
        <CardContent style={{ display: 'flex', alignItems: 'center' }}>
          <img src={randomImageURL} alt="Author" style={{ width: '100px', height: '100px', marginRight: '10px', borderRadius: '50%' }} />
          <div>
            <Typography variant="h6" gutterBottom>
              {blog.author}
            </Typography>
            <Typography variant="body1" sx={{ color: '#333', marginBottom: '12px' }}>
              {blog.message}
            </Typography>
          </div>
        </CardContent>
      </Card>
      </div>
    ));
  };

  return (
    <div>
         <Typography variant="h3" color="primary" gutterBottom style={{textAlign: 'center',marginTop: '40px'}}> 
          Blogs
        </Typography>
        <Divider style={{ width: '80%', backgroundColor: '#4A90E2', margin: 'auto', marginBottom: '20px' }} />
      {renderBlogs()}
      <div style={{ display: 'flex', justifyContent: 'center' }} className="mt-[-80px]">
        <Button variant="outlined" color="primary" >
          View More
        </Button>
      </div>
    </div>
  );
};

export default Blog;
