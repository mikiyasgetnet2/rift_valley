import React from "react";
import { FaFacebook } from "react-icons/fa";
import { FaTelegram } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";

const backgroundImage =
  "https://plus.unsplash.com/premium_photo-1683141170766-017bf7a2ecb4?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8ZG9uYXRpb258ZW58MHx8MHx8fDA%3D";

const Home = () => {
  return (
    <div
      style={{
        position: "relative",
        height: "690px",
        width: "100vw",
      }}
    >
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: "100%",
          zIndex: 1,
        }}
      >
        <div
          style={{
            background: "rgba(59, 89, 152, 0.8)",
            width: "50px",
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <FaFacebook style={{ fontSize: "24px", color: "white" }} />
        </div>
        <div
          style={{
            background: "rgba(0, 136, 204, 0.8)",
            width: "50px",
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <FaTelegram style={{ fontSize: "24px", color: "white" }} />
        </div>
        <div
          style={{
            background: "rgba(29, 161, 242, 0.8)",
            width: "50px",
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <FaTwitter style={{ fontSize: "24px", color: "white" }} />
        </div>
      </div>
      <div
        style={{
          backgroundImage: `linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(${backgroundImage})`,
          backgroundSize: "cover",
          height: "100%",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          color: "white",
          fontSize: "48px",
          fontWeight: "bold",
          textAlign: "center",
          textShadow: "2px 2px 4px rgba(0, 0, 0, 0.5)",
        }}
      >
        <div>
          Rift Valley Children and Women Development Organization (RCWDO)
          <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
          <div style={{borderRadius:"100%", width:"150px",opacity:"0.5", height:"150px", backgroundColor:"#80b3ff", display:"flex",alignItems:"center",justifyContent:"center"}}>
          Logo
          </div>

          </div>
        <div style={{ fontSize: "25px", marginTop: "24px", color: "#e6c300" }}>
            Empowering communities towards resilience and sustainable
            development.
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
