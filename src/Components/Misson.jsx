import React from 'react';
import { Typography, Divider } from '@mui/material';
import { FaCheck } from 'react-icons/fa';

const MissionVision = () => {
  return (
    <div className="flex flex-col md:flex-row justify-around">
      {/* Mission Section */}
      <div className="flex-1 bg-blue-500 p-4 text-center">
        <Typography variant="h4" className="text-white mb-4">
          Mission
        </Typography>
        <Divider className="w-full mx-auto mb-4" style={{ backgroundColor: 'gold' }} />
        <ul className='text-white'>
        <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          {/* Add other list items */}
        </ul>
      </div>
      {/* Vision Section */}
      <div className="flex-1 bg-[#3399ff] p-4 text-center">
        <Typography variant="h4" className="text-white mb-4">
          Vision
        </Typography>
        <Divider className="w-full mx-auto mb-4" style={{ backgroundColor: 'gold' }} />
        <ul className='text-white'>
        <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          <li className="mb-2 flex items-center p-4">
            <FaCheck className="text-lg mr-2" color='gold'/>
            <span className="text-lg">Empower communities for sustainable development</span>
          </li>
          {/* Add other list items */}
        </ul>
      </div>
    </div>
  );
};

export default MissionVision;
