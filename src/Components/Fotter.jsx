import React from 'react';
import Typography from '@mui/material/Typography';
import { FaFacebook, FaTelegram, FaTwitter } from 'react-icons/fa';

const Footer = () => {
  return (
    <div className="bg-gradient-to-br from-blue-500 to-blue-900 p-8 text-white flex flex-col justify-between h-52">
      <div className="flex justify-center">
        <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer" className="mr-4 flex items-center">
          <FaFacebook className="text-xl mr-1" />
          Facebook
        </a>
        <a href="https://www.telegram.me" target="_blank" rel="noopener noreferrer" className="mr-4 flex items-center">
          <FaTelegram className="text-xl mr-1" />
          Telegram
        </a>
        <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer" className="flex items-center">
          <FaTwitter className="text-xl mr-1" />
          Twitter
        </a>
      </div>
      <Typography variant="body2" className="text-center">
        &copy; {new Date().getFullYear()} Rift Valley Children and Women Development Organization. All Rights Reserved.
      </Typography>
    </div>
  );
};

export default Footer;
