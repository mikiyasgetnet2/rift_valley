import React from 'react';
import { FaPhone, FaEnvelope, FaMapMarkerAlt } from 'react-icons/fa';
import { Divider, Typography } from '@mui/material';

const Contact = () => {
  return (
    <div className="flex justify-center mt-24">
      {/* Contact Information Section */}
      <div className="p-2 text-center w-full md:w-1/2 ">
      <Typography variant="h3" color="primary" gutterBottom style={{textAlign: 'center',marginTop: '40px'}}> 
          Contact
        </Typography>
        <Divider style={{ width: '80%', margin: 'auto', color: '#0080FF' }} />
        <div className="mb-4 flex items-center justify-center p-4 mt-10">
          <FaPhone className="text-2xl text-blue-500 mr-2" />
          <p className="text-lg">+2519-123-4567</p>
        </div>
        <div className="mb-4 flex items-center justify-center p-4">
          <FaEnvelope className="text-2xl text-blue-500 mr-2" />
          <p className="text-lg">info@example.com</p>
        </div>
        <div className="flex items-center justify-center p-4">
          <FaMapMarkerAlt className="text-2xl text-blue-500 mr-2" />
          <p className="text-lg">123 Main St, City, Country</p>
        </div>
      </div>
      {/* Map Section */}
      <div className="p-2 text-center w-full md:w-1/2">
        {/* Replace the iframe with your actual map component */}
        <iframe
          title="Map"
          width="100%"
          height="400"
          frameBorder="0"
          scrolling="no"
          marginHeight="0"
          marginWidth="0"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.3538372627237!2d-74.00598238522442!3d40.71277664132045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2588f046ee661%3A0xe26f1fe47fbbcf14!2sEmpire%20State%20Building!5e0!3m2!1sen!2sus!4v1637406469379!5m2!1sen!2sus"
        ></iframe>
      </div>
    </div>
  );
};

export default Contact;
