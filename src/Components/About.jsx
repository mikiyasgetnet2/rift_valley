import React from 'react';

const About = () => {
  return (
    <div>
      <div style={{ 
        backgroundImage: 'linear-gradient(45deg, #6EB6FF, #4A90E2)',
        backgroundSize: 'cover', 
        height: '400px', 
        width: '100vw',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        fontSize: '24px',
        padding: '20px',
        textAlign: 'center'
      }}>
        <h1 style={{ fontSize: '50px', marginBottom: '20px' }}>About us</h1>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <p style={{fontSize: '17px',width: '90%', textAlign: 'center'}}>
        Rift Valley Children and Women Development Organization (RCWDO) is a national, indigenous, non-governmental, 
        not-for-profit and secular developmental charity organization. RCWDO promotes the welfare and development of 
        the most disadvantaged and vulnerable groups of society, mainly: children, women, youth, the elderly and people with disabilities 
        who are subjected to severe poverty and all forms of the problems of underdevelopment. RCWDO was founded in 1993 by dedicated, philanthropic 
        and voluntary Ethiopians residing in the Central Rift Valley area to address the critical causes of poverty in the rift valley area, as well as other parts of the country. RCWDO assists the efforts of the communities and affected people in strengthening humanitarian efforts. RCWDO envisions a poverty free, resilient society in which everyone can lead dignified quality of life, and experience equality, freedom and sustainable development in a healthy environment. RCWDO works for robust and thriving communities, particularly in the hard to reach and vulnerable segment of the society to enjoy sustainable 
        livelihoods with enhanced resilience capabilities to shocks and stress through their proactive engagement.
        </p>
        </div>
      </div>
    </div>
  );
};

export default About;
